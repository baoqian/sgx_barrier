# define MAX_PATH FILENAME_MAX

#include "sgx_urts.h"
#include "App.h"
#include "Enclave_u.h"
#include <chrono>

#include "../include/matrix/StaticMatrix.h"
#include "../include/multithread/SgxThreadGroup.h"
#include "Timer.h"
#include <memory>
#include <cstdlib>

/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

typedef struct _sgx_errlist_t {
	sgx_status_t err;
	const char *msg;
	const char *sug; /* Suggestion */
} sgx_errlist_t;

/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
	{
		SGX_ERROR_UNEXPECTED,
		"Unexpected error occurred.",
		NULL
	},
	{
		SGX_ERROR_INVALID_PARAMETER,
		"Invalid parameter.",
		NULL
	},
	{
		SGX_ERROR_OUT_OF_MEMORY,
		"Out of memory.",
		NULL
	},
	{
		SGX_ERROR_ENCLAVE_LOST,
		"Power transition occurred.",
		"Please refer to the sample \"PowerTransition\" for details."
	},
	{
		SGX_ERROR_INVALID_ENCLAVE,
		"Invalid enclave image.",
		NULL
	},
	{
		SGX_ERROR_INVALID_ENCLAVE_ID,
		"Invalid enclave identification.",
		NULL
	},
	{
		SGX_ERROR_INVALID_SIGNATURE,
		"Invalid enclave signature.",
		NULL
	},
	{
		SGX_ERROR_OUT_OF_EPC,
		"Out of EPC memory.",
		NULL
	},
	{
		SGX_ERROR_NO_DEVICE,
		"Invalid SGX device.",
		"Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
	},
	{
		SGX_ERROR_MEMORY_MAP_CONFLICT,
		"Memory map conflicted.",
		NULL
	},
	{
		SGX_ERROR_INVALID_METADATA,
		"Invalid enclave metadata.",
		NULL
	},
	{
		SGX_ERROR_DEVICE_BUSY,
		"SGX device was busy.",
		NULL
	},
	{
		SGX_ERROR_INVALID_VERSION,
		"Enclave version was invalid.",
		NULL
	},
	{
		SGX_ERROR_INVALID_ATTRIBUTE,
		"Enclave was not authorized.",
		NULL
	},
	{
		SGX_ERROR_ENCLAVE_FILE_ACCESS,
		"Can't open enclave file.",
		NULL
	},
	{
		SGX_ERROR_NDEBUG_ENCLAVE,
		"The enclave is signed as product enclave, and can not be created as debuggable enclave.",
		NULL
	},
	{
		SGX_ERROR_MEMORY_MAP_FAILURE,
		"Failed to reserve memory for the enclave.",
		NULL
	},
};

/* Check error conditions for loading enclave */
void print_error_message(sgx_status_t ret)
{
	size_t idx = 0;
	size_t ttl = sizeof sgx_errlist/sizeof sgx_errlist[0];

	for (idx = 0; idx < ttl; idx++) {
		if(ret == sgx_errlist[idx].err) {
			if(NULL != sgx_errlist[idx].sug)
				printf("Info: %s\n", sgx_errlist[idx].sug);
			printf("Error: %s\n", sgx_errlist[idx].msg);
			break;
		}
	}
	
	if (idx == ttl)
		printf("Error: Unexpected error occurred.\n");
}

/* Initialize the enclave:
 *   Call sgx_create_enclave to initialize an enclave instance
 */
int initialize_enclave(void)
{
	sgx_status_t ret = SGX_ERROR_UNEXPECTED;
	
	/* Call sgx_create_enclave to initialize an enclave instance */
	/* Debug Support: set 2nd parameter to 1 */
	ret = sgx_create_enclave(ENCLAVE_FILENAME, SGX_DEBUG_FLAG, NULL, NULL, &global_eid, NULL);
	if (ret != SGX_SUCCESS) {
		print_error_message(ret);
		return -1;
	}

	return 0;
}

/* OCall functions */
void ocall_print_string(const char *str)
{
	/* Proxy/Bridge will check the length and null-terminate 
	 * the input string to prevent buffer overflow. 
	 */
	printf("%s", str);
}

std::chrono::time_point<std::chrono::high_resolution_clock> t1;
double timess = 400;
void ocall_get_time_start() {
	t1 = std::chrono::high_resolution_clock::now();
}

void ocall_get_time_end() {
	auto t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> time_span = t2 - t1;
	double times = time_span.count();
	fprintf(stderr, "test took %f ms\n", times);
}

typedef char A_TYPE;
typedef short B_TYPE;
typedef int32_t C_TYPE;
typedef int32_t ProdType;
/* Application entry */
int SGX_CDECL main(int argc, char *argv[])
{
	(void)(argc);
	(void)(argv);
	int num_threads = 1;
	if (argc > 1) {
		num_threads = atoi(argv[1]);
	}

	/* Initialize the enclave */
	if(initialize_enclave() < 0){
		printf("Enter a character before exit ...\n");
		getchar();
		return -1;
	}

	static constexpr int A_ROW = 11008;
	static constexpr int A_COL = 4096;
	static constexpr int B_ROW = A_COL;
	static constexpr int C_ROW = A_ROW;

	std::unique_ptr<matrix::StaticMatrix<A_ROW, A_COL, A_TYPE>> a(new matrix::StaticMatrix<A_ROW, A_COL, A_TYPE>());
	for (int i = 0; i < A_ROW; ++i) {
		for (int j = 0; j < A_COL; ++j) {
			a->At(i, j) = i + j;
		}
	}

	std::unique_ptr<matrix::StaticColumnVector<B_ROW, B_TYPE>> b(new matrix::StaticColumnVector<B_ROW, B_TYPE>());
	for (int i = 0; i < B_ROW; ++i) {
		(*b)[i] = i;
	}

	std::unique_ptr<matrix::StaticColumnVector<C_ROW, C_TYPE>> c(new matrix::StaticColumnVector<C_ROW, C_TYPE>());
	for (int i = 0; i < C_ROW; ++i) {
		(*c)[i] = 0;
	}
	std::unique_ptr<matrix::StaticColumnVector<C_ROW, C_TYPE>> check_res(new matrix::StaticColumnVector<C_ROW, C_TYPE>());
	for (int i = 0; i < C_ROW; ++i) {
		(*check_res)[i] = 0;
	}

	for (int i = 0; i < A_ROW; ++i) {
		for (int j = 0; j < A_COL; ++j) {
			(*check_res)[i] += a->At(i, j) * (*b)[j];
		}
	}

	ecall_init_barrier(global_eid, num_threads);

	const multithread::ThreadGroup::WorkloadFunc workfunc = [&a, &b, &c](int thread_id,
			int num_threads, multithread::Barrier *barrier) {
		sgx_status_t ret = SGX_ERROR_UNEXPECTED;
		ret = ecall_test_sgx_barrier(global_eid, thread_id, num_threads, (void *)(a.get()), (void *)(b.get()), (void *)(c.get()));
		if (ret != SGX_SUCCESS)
			abort();
	};

	std::unique_ptr<multithread::ThreadGroup> thread_group(
			new multithread::SgxThreadGroup(num_threads, workfunc));
	{
		Timer timer;
		timer << A_ROW << "*" << A_COL << " x " << B_ROW << "*1, "
				<< num_threads << " thread";
		thread_group->Start();
		thread_group->Join();
	}

	for (int i = 0; i < C_ROW; ++i) {
		if ((*c)[i] != (*check_res)[i]) {
			printf("error @%d expect %d vs %d\n", i, (*check_res)[i], (*c)[i]);
			break;
		}
	}
	/* Destroy the enclave */
	sgx_destroy_enclave(global_eid);
	
	printf("Info: successfully returned.\n");

	return 0;
}

// ./app m k n