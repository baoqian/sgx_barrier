/*
 * SgxThreadGroup.cpp
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#include "../include/multithread/SgxThreadGroup.h"

namespace multithread {

SgxThreadGroupBase::SgxThreadGroupBase(int num_threads) :
		ThreadGroup(num_threads),
		threads_(num_threads, nullptr) {}

SgxThreadGroupBase::~SgxThreadGroupBase() {
	for (auto *thread : threads_) {
		if (thread != nullptr) {
			delete thread;
		}
	}
}

void SgxThreadGroupBase::Start() {
	for (int thread_id = 0; thread_id < NumThreads(); ++thread_id) {
		threads_[thread_id] = new std::thread([this, thread_id]() {
			Workload(thread_id, NumThreads(), nullptr);
		});
	}
}

void SgxThreadGroupBase::Join() {
	for (int thread_id = 0; thread_id < NumThreads(); ++thread_id) {
		auto *thread = threads_[thread_id];
		if (thread != nullptr) {
			thread->join();
		}
	}
}

SgxThreadGroup::SgxThreadGroup(int num_threads, const WorkloadFunc &func) :
		SgxThreadGroupBase(num_threads), func_(func) {
}

SgxThreadGroup::~SgxThreadGroup() {}

void SgxThreadGroup::Workload(int thread_id, int num_threads,
		Barrier *barrier) {
	func_(thread_id, num_threads, nullptr);
}

} /* namespace multithread */
