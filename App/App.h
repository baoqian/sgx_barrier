#ifndef _APP_H_
#define _APP_H_

#include <stdio.h>
#include <memory>
#include "sgx_error.h"	   /* sgx_status_t */
#include "sgx_eid.h"	 /* sgx_enclave_id_t */

#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif

#if   defined(__GNUC__)
# define ENCLAVE_FILENAME "enclave.signed.so"
#endif

extern sgx_enclave_id_t global_eid;	/* global enclave id */

#if defined(__cplusplus)
extern "C" {
#endif

void test_sgx_barrier(int num_threads);

#if defined(__cplusplus)
}
#endif

#endif /* !_APP_H_ */