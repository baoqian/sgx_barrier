/*
 * SgxBarrier.cpp
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#include "../include/multithread/SgxBarrier.h"

namespace multithread {

SgxBarrier::SgxBarrier(int num_threads) : Barrier(num_threads), num_threads(num_threads), count(0), release(0) {}

SgxBarrier::~SgxBarrier() {}

void SgxBarrier::Wait() {
	sgx_thread_mutex_lock(&mutex);
	if (count == 0) {
		release = 0;
	}
	count++;
	if (count == num_threads) {
		count = 0;
		release = 1;
		sgx_thread_cond_broadcast(&condition); // notify_all
	} else {
		while (release != 1)
			sgx_thread_cond_wait(&condition, &mutex);
	}
	sgx_thread_mutex_unlock(&mutex);
}

} /* namespace multithread */
