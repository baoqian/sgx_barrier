#include "../Enclave.h"
#include "Enclave_t.h"
#include "sgx_thread.h"
#include "../../include/multithread/SgxBarrier.h"
#include "../../include/matrix/StaticMatrix.h"

typedef char A_TYPE;
typedef short B_TYPE;
typedef int32_t C_TYPE;
typedef int32_t ProdType;

static constexpr int A_ROW = 11008;
static constexpr int A_COL = 4096;
static constexpr int B_ROW = A_COL;
static constexpr int C_ROW = A_ROW;

multithread::SgxBarrier *sgx_barrier = nullptr;

void ecall_init_barrier(int num_threads) {
    static multithread::SgxBarrier instance(num_threads);
    sgx_barrier = &instance;
}

void ecall_test_sgx_barrier(int thread_id, int num_threads, void *static_matrix_a, void *static_matrix_b, void *static_matrix_c) {
    printf("In thread %d / %d\n", thread_id, num_threads);
    sgx_barrier->Wait();
	matrix::StaticMatrix<A_ROW, A_COL, A_TYPE> *a = (matrix::StaticMatrix<A_ROW, A_COL, A_TYPE> *)static_matrix_a;

	matrix::StaticColumnVector<B_ROW, B_TYPE> *b = (matrix::StaticColumnVector<B_ROW, B_TYPE> *)static_matrix_b;

	matrix::StaticColumnVector<C_ROW, C_TYPE> *c = (matrix::StaticColumnVector<C_ROW, C_TYPE> *)static_matrix_c;

    if (num_threads == 1) {
        get_time_start();
        a->MatVecMul<ProdType, 4, B_ROW, B_TYPE, C_ROW, C_TYPE>(*b, c);
        get_time_end();
    } else {
        get_time_start();
        a->MatVecMulMultiThread<ProdType, 4, B_ROW, B_TYPE, C_ROW, C_TYPE>(*b, c, thread_id, num_threads);
        get_time_end();
    }
}