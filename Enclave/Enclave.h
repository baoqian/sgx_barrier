#ifndef _ENCLAVE_H_
#define _ENCLAVE_H_

#include <stdlib.h>
#include <assert.h>

#if defined(__cplusplus)
extern "C" {
#endif

int printf(const char *fmt, ...);
void get_time_start();
void get_time_end();

#if defined(__cplusplus)
}
#endif

#endif /* !_ENCLAVE_H_ */
