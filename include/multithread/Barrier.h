/*
 * Barrier.h
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_MULTITHREAD_BARRIER_H_
#define SRC_INCLUDE_MULTITHREAD_BARRIER_H_

namespace multithread {

class Barrier {
public:
	Barrier(int num_threads) {}
	virtual ~Barrier() {}

	virtual void Wait() = 0;

private:
	Barrier(const Barrier &other) = delete;
	Barrier(Barrier &&other) = delete;
	Barrier& operator=(const Barrier &other) = delete;
	Barrier& operator=(Barrier &&other) = delete;
};

} /* namespace multithread */

#endif /* SRC_INCLUDE_MULTITHREAD_BARRIER_H_ */
