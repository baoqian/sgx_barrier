/*
 * SgxBarrier.h
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_MULTITHREAD_SGXBARRIER_H_
#define SRC_INCLUDE_MULTITHREAD_SGXBARRIER_H_

#include "Barrier.h"
#include "sgx_thread.h"

namespace multithread {

class SgxBarrier : public Barrier {
public:
	SgxBarrier(int num_threads);
	~SgxBarrier() override;

	void Wait() override;

private:
	SgxBarrier(const SgxBarrier &other) = delete;
	SgxBarrier(SgxBarrier &&other) = delete;
	SgxBarrier& operator=(const SgxBarrier &other) = delete;
	SgxBarrier& operator=(SgxBarrier &&other) = delete;

private:
	int num_threads = 0;
	int count = 0;
	int release = 0;
	sgx_thread_mutex_t mutex = SGX_THREAD_MUTEX_INITIALIZER;
	sgx_thread_cond_t condition = SGX_THREAD_COND_INITIALIZER;
};

} /* namespace multithread */

#endif /* SRC_INCLUDE_MULTITHREAD_SGXBARRIER_H_ */
