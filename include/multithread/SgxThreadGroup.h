/*
 * SgxThreadGroup.h
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_MULTITHREAD_STDTHREADGROUP_H_
#define SRC_INCLUDE_MULTITHREAD_STDTHREADGROUP_H_

#include "ThreadGroup.h"
#include <thread>
#include <vector>

namespace multithread {

class SgxThreadGroupBase : public ThreadGroup {
public:
	SgxThreadGroupBase(int num_threads);
	~SgxThreadGroupBase() override;

	void Start() override;
	void Join() override;

private:
	SgxThreadGroupBase(const SgxThreadGroupBase &other) = delete;
	SgxThreadGroupBase(SgxThreadGroupBase &&other) = delete;
	SgxThreadGroupBase& operator=(const SgxThreadGroupBase &other) = delete;
	SgxThreadGroupBase& operator=(SgxThreadGroupBase &&other) = delete;

	std::vector<std::thread *> threads_;
};

class SgxThreadGroup : public SgxThreadGroupBase {
public:
	SgxThreadGroup(int num_threads, const WorkloadFunc &func);
	~SgxThreadGroup() override;

protected:
	void Workload(int thread_id, int num_threads, Barrier *barrier) override;

private:
	WorkloadFunc func_;
};

} /* namespace multithread */

#endif /* SRC_INCLUDE_MULTITHREAD_STDTHREADGROUP_H_ */
