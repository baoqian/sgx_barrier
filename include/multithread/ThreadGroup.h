/*
 * ThreadGroup.h
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_MULTITHREAD_THREADGROUP_H_
#define SRC_INCLUDE_MULTITHREAD_THREADGROUP_H_

#include "Barrier.h"
#include <functional>

namespace multithread {

class ThreadGroup {
public:
	typedef std::function<void(int, int, Barrier *)> WorkloadFunc;

	ThreadGroup(int num_threads) : num_threads_(num_threads) {}
	virtual ~ThreadGroup() {}

	int NumThreads() const { return num_threads_; }
	virtual void Start() = 0;
	virtual void Join() = 0;

protected:
	virtual void Workload(int thread_id, int num_threads, Barrier *barrier) = 0;

private:
	ThreadGroup(const ThreadGroup &other) = delete;
	ThreadGroup(ThreadGroup &&other) = delete;
	ThreadGroup& operator=(const ThreadGroup &other) = delete;
	ThreadGroup& operator=(ThreadGroup &&other) = delete;

	int num_threads_;
};

} /* namespace multithread */

#endif /* SRC_INCLUDE_MULTITHREAD_THREADGROUP_H_ */
