/*
 * Matrix.h
 *
 *  Created on: 2024��1��5��
 *      Author: jesse
 */

#ifndef INCLUDE_MATRIX_MATRIX_H_
#define INCLUDE_MATRIX_MATRIX_H_

#include <vector>
#include <iostream>
#include <sstream>

namespace matrix {

template<class _DataType>
class MatrixBase {
public:
	typedef _DataType DataType;

	MatrixBase() : num_rows_(0), num_columns_(0) {}

	MatrixBase(std::size_t num_rows, std::size_t num_columns) :
			num_rows_(num_rows), num_columns_(num_columns),
			data_(num_rows * num_columns) {
	}

	std::size_t NumRows() const { return num_rows_; }
	std::size_t NumColumns() const { return num_columns_; }

	void resize(std::size_t num_rows, std::size_t num_columns) {
		this->num_rows_ = num_rows;
		this->num_columns_ = num_columns;
		data_.resize(num_rows * num_columns);
	}

	DataType *data() { return data_.data(); }
	const DataType *data() const { return data_.data(); }

private:
	std::size_t num_rows_;
	std::size_t num_columns_;
	std::vector<DataType> data_;
};

template<class _DataType>
class MatrixRowMajor : public MatrixBase<_DataType> {
public:
	typedef _DataType DataType;
	typedef MatrixBase<DataType> BaseType;

	MatrixRowMajor() : BaseType() {}

	MatrixRowMajor(std::size_t num_rows, std::size_t num_columns) :
			BaseType(num_rows, num_columns) {
	}

	DataType& At(std::size_t row, std::size_t column) {
		return BaseType::data()[row * BaseType::NumColumns() + column];
	}

	const DataType& At(std::size_t row, std::size_t column) const {
		return BaseType::data()[row * BaseType::NumColumns() + column];
	}
};

template<class _DataType>
class MatrixColumnMajor : public MatrixBase<_DataType> {
public:
	typedef _DataType DataType;
	typedef MatrixBase<DataType> BaseType;

	MatrixColumnMajor() : BaseType() {}

	MatrixColumnMajor(std::size_t num_rows, std::size_t num_columns) :
			BaseType(num_rows, num_columns) {
	}

	DataType& At(std::size_t row, std::size_t column) {
		return BaseType::data()[column * BaseType::NumRows() + row];
	}

	const DataType& At(std::size_t row, std::size_t column) const {
		return BaseType::data()[column * BaseType::NumRows() + row];
	}
};

template<class DataType, bool RowMajor = true>
class Matrix {
	// Empty
};

template<class _DataType>
class Matrix<_DataType, true> : public MatrixRowMajor<_DataType> {
public:
	static constexpr bool RowMajor = true;
	typedef _DataType DataType;
	typedef MatrixRowMajor<DataType> BaseType;

	Matrix() : BaseType() {}

	Matrix(std::size_t num_rows, std::size_t num_columns) :
			BaseType(num_rows, num_columns) {
	}
};

template<class _DataType>
class Matrix<_DataType, false> : public MatrixColumnMajor<_DataType> {
public:
	static constexpr bool RowMajor = false;
	typedef _DataType DataType;
	typedef MatrixColumnMajor<DataType> BaseType;

	Matrix() : BaseType() {}

	Matrix(std::size_t num_rows, std::size_t num_columns) :
			BaseType(num_rows, num_columns) {
	}
};

template<class DataType, bool RowMajor>
inline std::ostream &operator<<(std::ostream &os, const Matrix<DataType, RowMajor> &matrix) {
	std::stringstream strout;
	strout << "[";
	for (unsigned i = 0; i < matrix.NumRows(); ++i) {
		if (i > 0) {
			strout << "," << std::endl;
		}
		strout << "[";
		for (unsigned j = 0; j < matrix.NumColumns(); ++j) {
			if (j > 0) {
				strout << ", ";
			}
			strout << matrix.At(i, j);
		}
		strout << "]";
	}
	strout << "]";
	os << strout.str();
	return os;
}

} /* namespace matrix */

#endif /* INCLUDE_MATRIX_MATRIX_H_ */
