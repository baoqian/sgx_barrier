/*
 * StaticMatrix.h
 *
 *  Created on: 2024年1月8日
 *      Author: xing
 */

#ifndef SRC_INCLUDE_MATRIX_STATICMATRIX_H_
#define SRC_INCLUDE_MATRIX_STATICMATRIX_H_

#include "util.h"

namespace matrix {

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixBase {
public:
	typedef _DataType DataType;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	DataType *data() { return data_; }
	const DataType *data() const { return data_; }

protected:
	DataType data_[NRows * NCols];
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixRowMajor : public StaticMatrixBase<_NRows, _NCols, _DataType> {
public:
	static const int NRows = _NRows;
	static const int NCols = _NCols;
	typedef _DataType DataType;
	typedef StaticMatrixBase<NRows, NCols, DataType> BaseType;

	DataType& At(int row, int column) {
		return BaseType::data_[row * NCols + column];
	}

	const DataType& At(int row, int column) const {
		return BaseType::data_[row * NCols + column];
	}
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrixColumnMajor : public StaticMatrixBase<_NRows, _NCols, _DataType> {
public:
	static const int NRows = _NRows;
	static const int NCols = _NCols;
	typedef _DataType DataType;
	typedef StaticMatrixBase<NRows, NCols, DataType> BaseType;

	DataType& At(int row, int column) {
		return BaseType::data_[column * NRows + column];
	}

	const DataType& At(int row, int column) const {
		return BaseType::data_[column * NRows + column];
	}
};

template<int NRows, int NCols, class DataType, bool RowMajor = true>
class StaticMatrix {
	// Empty
};

template<int _NRows, class _DataType, bool _RowMajor>
class StaticColumnVector;

template<int _NRows, int _NCols, class _DataType>
class StaticMatrix<_NRows, _NCols, _DataType, true> : public StaticMatrixRowMajor<_NRows, _NCols, _DataType> {
public:
	static constexpr bool RowMajor = true;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrixRowMajor<_NRows, _NCols, _DataType> BaseType;

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, int _OtherNCols, class _OtherDataType, bool _OtherRowMajor,
			int _ResNRows, int _ResNCols, class _ResDataType, bool _ResRowMajor>
	void MatMul(const StaticMatrix<_OtherNRows, _OtherNCols, _OtherDataType, _OtherRowMajor> &other,
			StaticMatrix<_ResNRows, _ResNCols, _ResDataType, _ResRowMajor> *result) const {}

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatMul(const StaticMatrix<_OtherNRows, 1, _OtherDataType, true> &other,
			StaticMatrix<_ResNRows, 1, _ResDataType, true> *result, int star_row, int end_row) const {
		const int chunk_size = CeilDiv(NCols, _MatrixBSplit);
		for (int a_row = star_row; a_row < end_row && a_row < NRows; ++a_row) {
			_ProdType sum = 0;
			for (int i = 0; i < _MatrixBSplit; ++i) {
				const int chunk_offset = chunk_size * i;
				const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
				for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
					sum += Prod<_DataType, _OtherDataType, _ProdType>(this->At(a_row, j), other.At(j, 0));
				}
			}
			result->At(a_row, 0) = sum;
		}
	}

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatVecMul(const StaticColumnVector<_OtherNRows, _OtherDataType, true> &vec,
			StaticColumnVector<_ResNRows, _ResDataType, true> *result) const {
		MatMul<_ProdType, _MatrixBSplit, _OtherNRows, _OtherDataType, _ResNRows, _ResDataType>(vec, result, 0, NRows);
	}

	template<class _ProdType, int _MatrixBSplit, int _OtherNRows, class _OtherDataType,
			int _ResNRows, class _ResDataType>
	void MatVecMulMultiThread(const StaticMatrix<_OtherNRows, 1, _OtherDataType, true> &other,
			StaticMatrix<_ResNRows, 1, _ResDataType, true> *result, int thread_id, int num_threads) const {
		const int row_chunk_size = CeilDiv(NRows, num_threads);
		const int start_row = thread_id * row_chunk_size;
		const int row_size = (NRows - start_row > row_chunk_size) ? row_chunk_size : (NRows - start_row);
		const int end_row = start_row + row_size;

		const int chunk_size = CeilDiv(NCols, _MatrixBSplit);
		for (int a_row = start_row; a_row < end_row && a_row < NRows; ++a_row) {
			_ProdType sum = 0;
			for (int i = 0; i < _MatrixBSplit; ++i) {
				const int chunk_offset = chunk_size * i;
				const int chunks = (NCols - chunk_offset > chunk_size) ? chunk_size : (NCols - chunk_offset);
				for (int j = chunk_offset; j < chunk_offset + chunks && j < NCols; ++j) {
					sum += Prod<_DataType, _OtherDataType, _ProdType>(this->At(a_row, j), other.At(j, 0));
				}
			}
			result->At(a_row, 0) = sum;
		}
	}
};

template<int _NRows, int _NCols, class _DataType>
class StaticMatrix<_NRows, _NCols, _DataType, false> : public StaticMatrixColumnMajor<_NRows, _NCols, _DataType> {
public:
	static constexpr bool RowMajor = false;
	static const int NRows = _NRows;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrixColumnMajor<_NRows, _NCols, _DataType> BaseType;
};

template<int _NRows, class _DataType, bool _RowMajor = true>
class StaticColumnVector : public StaticMatrix<_NRows, 1, _DataType, _RowMajor> {
public:
	static constexpr bool RowMajor = _RowMajor;
	static const int NRows = _NRows;

	typedef _DataType DataType;
	typedef StaticMatrix<NRows, 1, DataType, RowMajor> BaseType;

	DataType &operator[](int idx) {
		return BaseType::At(idx, 0);
	}

	const DataType &operator[](int idx) const {
		return BaseType::At(idx, 0);
	}
};

template<int _NCols, class _DataType, bool _RowMajor = true>
class StaticRowVector : public StaticMatrix<1, _NCols, _DataType, _RowMajor> {
public:
	static constexpr bool RowMajor = _RowMajor;
	static const int NCols = _NCols;

	typedef _DataType DataType;
	typedef StaticMatrix<1, NCols, DataType, RowMajor> BaseType;

	DataType &operator[](int idx) {
		return BaseType::At(0, idx);
	}

	const DataType &operator[](int idx) const {
		return BaseType::At(0, idx);
	}
};

} /* namespace matrix */

#endif /* SRC_INCLUDE_MATRIX_STATICMATRIX_H_ */
